VT IT Platform Workstation Container Image
====================================

This project builds a container image with the tools to enable a workstation-like environment in a container.

The requirements.txt file includes a list of packages to be installed with pip3 as the user running the
workstation image.

To run the container using kubernetes, edit the "workstation-pod.yml" file to update the hostPath and mountPath, as well as the USERNAME, UIDNUMBER, and SHELL env variables as desired . Once the file has been updated as required, simply run:

`kubectl create -f workstation-pod.yml`

**NOTE** If you are using the docker engine instead of containerd, you will need to use the docker login command to gain access to the container registry. This appears to be a bug in gitlab as of this writing, and to allow the desired "public" access that was configured on the repository, you can run the following command for read access on your docker system:

`docker login -u username-workstation-readonly -p code-vt-edu-usertoken code.vt.edu:5005`

An example of an updated workstation-pod.yml file follows, for the user "vtuser" on a Mac OS system.

```
apiVersion: v1
kind: Pod
metadata:
  name: workstation
spec:
  volumes:
    - name: home-mount
      hostPath:
        path: /Users/vtuser
  containers:
  - name: workstation
    image: code.vt.edu:5005/it-common-platform/public-images/workstation:latest
    securityContext:
       capabilities:
         add:
           - NET_ADMIN
    env:
    - name: USERNAME
      value: "vtuser"
    - name: UIDNUMBER
      value: "501"
    - name: SHELL
      value: "/bin/bash"
    volumeMounts:
      - name: home-mount
        mountPath: /home/vtuser
    # Just spin & wait forever
    command: [ "/bin/bash", "-c", "/entrypoint.sh" ]
```

Once the container is running, you can then run the following commands to exec into the container, then change to your user:

```
> kubectl exec --stdin --tty workstation -- /bin/bash -c "su - vtuser"
```

The sudo password for the newly created user can be found in /sudo_password.txt in the container.

To setup a VPN connection from within the container itself, follow the directions at: https://vtluug.org/wiki/Proxies_and_VPN . If you prefer "alltraffic" VPN to be used, simply change the end of the URI from "vttraffic" to "alltraffic" in the openconnect command.

Additionally, it is recommended to use a different KUBECONFIG location within the container, so that your KUBECONFIG does not
conflict with your host system's configuration. Adding the following to your .bashrc(or other profile options, depending on
the shell utilized within the container, as long as it is not the same shell used as the host system):

```
export KUBECONFIG="${HOME}/.kube/workstation-config"
```

### Docker Engine Only

To run the container directly with the docker engine(without kubernetes), modify the following example to your needs/environment:

`docker run -d --cap-add=NET_ADMIN --env USERNAME=vtuser --env UIDNUMBER=501 --env shell="/bin/bash" -v /Users/vtuser:/home/vtuser --name workstation code.vt.edu:5005/it-common-platform/public-images/workstation:latest`

Then, simply run the following command to exec into the container(changing "vtuser" to your username):

`docker exec -it workstation /bin/bash -c "su - vtuser"`

**NOTE** You will receive an WARNING message, however the container will still run. Also, see the note above in the README related to the docker login requirement when utilizing the docker engine.
