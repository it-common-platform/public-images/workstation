aws_sts_set () {
  # Example usage:
  # aws_sts_set <master-account> <account-to-be-managed> <role-name>
  # aws_sts_set vtnis-ss 541585145005 NISAdmin
  unset AWS_ACCESS_KEY_ID
  unset AWS_SECRET_ACCESS_KEY
  unset AWS_SESSION_TOKEN
  AWS_ACCOUNT_NUMBER=$2
  AWS_ROLE_NAME=$3
  export AWS_ACCESS_KEY_ID=$(gpg --quiet -d ${HOME}/.aws/$1_id.asc)
  export AWS_SECRET_ACCESS_KEY=$(gpg --quiet -d ${HOME}/.aws/$1_key.asc)
  token=$(aws sts assume-role --role-arn "arn:aws:iam::${AWS_ACCOUNT_NUMBER}:role/$AWS_ROLE_NAME" --role-session-name $USER-$AWS_ROLE_NAME-workstation --duration-seconds 3600 )
  unset AWS_ACCESS_KEY_ID
  unset AWS_SECRET_ACCESS_KEY
  unset AWS_SESSION_TOKEN

  id=$(echo $token | jq --raw-output '.Credentials.AccessKeyId')
  key=$(echo $token | jq --raw-output '.Credentials.SecretAccessKey')
  session_token=$(echo $token | jq --raw-output '.Credentials.SessionToken')

  export AWS_ACCESS_KEY_ID=$id
  export AWS_SECRET_ACCESS_KEY=$key
  export AWS_SESSION_TOKEN=$session_token
}

aws_do () {

  AWS_ACCESS_KEY_ID=$(gpg --quiet -d ${HOME}/.aws/$1_id.asc) \
  AWS_SECRET_ACCESS_KEY=$(gpg --quiet -d ${HOME}/.aws/$1_key.asc) \
  ${@:2}

}

aws_sts_do () {
  # Example usage:
  # aws_sts_do <master-account> <account-to-be-managed> <role-name> <commands>
  # aws_sts_do vtnis-ss 541585145005 NISAdmin terraform plan
  AWS_ACCOUNT_NUMBER=$2
  AWS_ROLE_NAME=$3
  export AWS_ACCESS_KEY_ID=$(gpg --quiet -d ${HOME}/.aws/$1_id.asc)
  export AWS_SECRET_ACCESS_KEY=$(gpg --quiet -d ${HOME}/.aws/$1_key.asc)
  token=$(aws sts assume-role --role-arn "arn:aws:iam::${AWS_ACCOUNT_NUMBER}:role/$AWS_ROLE_NAME" --role-session-name $USER-$AWS_ROLE_NAME-workstation --duration-seconds 900 )
  unset AWS_ACCESS_KEY_ID
  unset AWS_SECRET_ACCESS_KEY
  unset AWS_SESSION_TOKEN

  id=$(echo $token | jq --raw-output '.Credentials.AccessKeyId')
  key=$(echo $token | jq --raw-output '.Credentials.SecretAccessKey')
  session_token=$(echo $token | jq --raw-output '.Credentials.SessionToken')

  AWS_ACCESS_KEY_ID=$id \
  AWS_SECRET_ACCESS_KEY=$key \
  AWS_SESSION_TOKEN=$session_token \
  ${@:4}

}
